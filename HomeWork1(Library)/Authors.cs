﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class Authors : IComparable<Authors>, ICountingBooks
    {
        public string Name { get; private set; }
        public int NumOfBooks { get; set; }
        public Authors(string name)
        {
            Name = name;
        }

        public int CompareTo(Authors other)
        {
            return this.NumOfBooks.CompareTo(other.NumOfBooks);
        }

        public int Count()
        {
            return NumOfBooks;
        }
    }
}
