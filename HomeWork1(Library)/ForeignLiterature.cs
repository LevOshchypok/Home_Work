﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class ForeignLiterature : Department
    {
        public ForeignLiterature(int sz, params Book[] b) : base(sz, b)
        {
            base.Name = "ForeignLiterature";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("ForeignLiterature departmen has {0} books", base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }
    }

}
