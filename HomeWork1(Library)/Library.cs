﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class Library : ICountingBooks
    {
        public Department[] departmens;
        int size;


        public Library(int sz)
        {
            size = sz;
            departmens = new Department[size];
        }

        public override string ToString()
        {
            return string.Format("Our new library");
        }
        public void Display()
        {
            foreach (Department d in departmens)
            {
                d.Display();
                Console.WriteLine();
            }

        }

        public int Count()
        {
            int Sum = 0;

            foreach (Department d in departmens)
            {
                Sum += d.Count();
            }
            return Sum;
        }

    }
}
