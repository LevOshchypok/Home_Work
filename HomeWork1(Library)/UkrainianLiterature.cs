﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class UkrainianLiterature : Department
    {
        public UkrainianLiterature(int sz, params Book[] b) : base(sz,b)
        {
            base.Name = "UkrainianLiterature";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("UkrainianLiterature departmen has {0} books", base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }

    }
}
