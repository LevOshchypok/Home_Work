﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class Book : IComparable<Book>
    {
        public string Name { get; private set; }
        public Authors Author { get; private set; }

        public int NumOfPages { get; private set; }

        public Book() { }
        public Book(int np) : this("", null, np) { }
        public Book(string name, Authors author, int np)
        {
            Name = name;
            Author = author;
            NumOfPages = np;
            if (Author != null)
            {
                Author.NumOfBooks++;
            }
        }
        public override string ToString()
        {

            string str = "Name of book -- " + this.Name + " .Author -" + this.Author.Name + ". Amount of pages - " + this.NumOfPages.ToString();
            return str;
        }

        public int CompareTo(Book other)
        {
            return this.NumOfPages.CompareTo(other.NumOfPages);
        }
    }
}
