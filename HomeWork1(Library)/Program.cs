﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Library OurLibrary = new Library(3);

            Department IT = new IT(3);
            Department UkrLIt = new UkrainianLiterature(4);
            Department ForLit = new ForeignLiterature(2);

            Authors Shevchenko = new Authors("Shevchenko");
            Authors Remark = new Authors("Remark");
            Authors Troelsten = new Authors("Troelsten");

            Book[] ProgBooks = new Book[3] {
                new Book("Programiring 1", Troelsten, 900),
                new Book("Programiring 2", Troelsten,800),
                new Book("Programiring 3", Troelsten, 770)};

            Book[] UkrLitBooks = new Book[4]
            {
                new Book("Kobzar 1", Shevchenko, 700),
                 new Book("Kobzar 2", Shevchenko, 800),
                  new Book("Kobzar 3", Shevchenko, 100),
                   new Book("Kobzar 4", Shevchenko, 256)
            };
            Book[] ForeignBooks = new Book[2] {
                new Book("Three friends", Remark, 345),
                new Book("Three friends 1", Remark, 256)
            };

            IT.Books = ProgBooks;
            UkrLIt.Books = UkrLitBooks;
            ForLit.Books = ForeignBooks;

            OurLibrary.departmens[0]  = IT;
            OurLibrary.departmens[1] = UkrLIt;
            OurLibrary.departmens[2] = ForLit;

            Console.WriteLine(OurLibrary.Count());
            OurLibrary.Display();
            Console.WriteLine(Shevchenko.Count());

            //Additional task
            Console.WriteLine("Additional task");
            Console.WriteLine();

            MaxNumberBooksInAuthor(Shevchenko, Remark, Troelsten);

            Console.WriteLine();
            MaxNumberBooksInDepartment(IT, UkrLIt, ForLit);

            Console.WriteLine();
            MinPageInBook(OurLibrary);

            Console.ReadKey();
        }
        static void MaxNumberBooksInAuthor(params Authors[] authors)
        {
            if (authors.Length != 0)
            {
                Authors A1 = authors[0];
                for (int i = 0; i < authors.Length; i++)
                {
                    if (A1.CompareTo(authors[i]) < 0)
                    {
                        A1 = authors[i];
                    }
                }
                Console.WriteLine(A1.Name + "Have the most books");
            }
            else { Console.WriteLine("You didn't input ane author"); }
        }
        static void MaxNumberBooksInDepartment(params Department[] dep)
        {
            if (dep.Length != 0)
            {
                Department D = dep[0];
                for (int i = 0; i < dep.Length; i++)
                {
                    if (D.CompareTo(dep[i]) < 0)
                    {
                        D = dep[i];
                    }
                }
                Console.WriteLine("Department of " + D.Name + " have the most books");
            }
            else { Console.WriteLine("You didn't input ane Department"); }

        }
        static void MinPageInBook(Library lib)
        {
            // Books with the lowest number of pages in each department
            Book[] books = new Book[lib.departmens.Length];
            // Book with the lowest number of pages
            Book book = new Book();
            for (int i = 0; i < lib.departmens.Length; i++)
            {
                for (int j = 0; j < lib.departmens[i].Books.Length; j++)
                {
                    book = lib.departmens[i].Books[0];
                    if (book.CompareTo(lib.departmens[i].Books[j]) > 0)
                    {
                        book = lib.departmens[i].Books[j];
                        books[i] = book;
                    }
                }
            }
            for (int g = 0; g < lib.departmens.Length; g++)
            {
                book = books[0];
                if (book.CompareTo(books[g]) > 0)
                {
                    book = books[g];
                }
            }
            Console.WriteLine("The book with the lowest number of pages is " + book.Name + " with " + book.NumOfPages + " pages.");
        }
    }
}
