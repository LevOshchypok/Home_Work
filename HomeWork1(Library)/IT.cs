﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    class IT : Department
    {
        public IT(int sz, params Book[] b) : base(sz,b)
        {
            base.Name = "IT";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("IT departmen has {0} books",base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }
    }
}
