﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_1
{
    abstract class Department : IComparable<Department>, ICountingBooks
    {
        public string Name { get; protected set; }
        public Book[] Books { get; set; }
        int size;
        private int sz;
        private Book[] b;

        public Department() { }
        public Department(int sz, params Book[] b) : this("", sz, b) { }
        public Department(string name) : this(name,0, null) { }
        public Department(params Book[] b) : this("",0, b) { }
        public Department(string name,int sz, params Book[] b)
        {
            size = sz;
            Books = new Book[size];
            this.Name = name;
            this.Books = b;
        }

        //public Department(int sz, Book[] b)
        //{
        //    this.sz = sz;
        //    this.b = b;
        //}

        public virtual int NumOfBooks()
        {
            return this.Books.Length;
        }
        public override string ToString()
        {
            return string.Format("The {0} has {1} books", Name, this.NumOfBooks().ToString());
        }
        public void Display()
        {
            Console.WriteLine(this.ToString());
            Console.WriteLine("Such AS");
            foreach (Book b in Books)
            {
                Console.WriteLine(b.ToString());
            }
        }

        public int CompareTo(Department other)
        {
            return this.Books.Length.CompareTo(other.Books.Length);
        }

        public int Count()
        {
            return Books.Length;
        }
    }       
}
