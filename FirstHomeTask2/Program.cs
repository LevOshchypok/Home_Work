﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHomeTask2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 4; // number of rows
            int m = 5; // number of columns
            int[,] array = new int[n, m];
            Random r = new Random();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = r.Next(-20, 30);
                }
            }
            // Задаю інший масив для перевірки
            #region 
            int[,] arr2 = new int[4,5] { {1,2,3,4,5 },
                                            {1,2,2,2,5 },
                                            { 3,4,3,5,7},
                                             { 4,4,4,4,8}   };

            #endregion

            Console.WriteLine("Our array looks like ");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }

            FirstColumnWithoutNegativeElem(array, n, m);
            SortRows(arr2, n, m);
                       
          
            Console.Read();

        }
        private static void Switch(ref int[] a,ref int[] b)
            {
            if (a.Length == b.Length)
            {
                int[] t = new int[a.Length];
                t = a;
                a = b;
                b = t; 
            }
            }
        private static void FirstColumnWithoutNegativeElem(int[,] array, int n, int m)
        {
            bool flag = false;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (array[j, i] < 0)
                    {
                        flag = false;
                        break;
                    }
                    else { flag = true; }
                }
                if (flag)
                {
                    Console.WriteLine("The first column without negative elements is " + i);
                    break;
                }
            }
        }
        private static void SortRows(int[,] array, int n, int m)
        {
            // Задаю Масив
            int[] k = new int[n];
            int[][] AddArr = new int[n][];
            for (int i = 0; i < n; i++)
            {
                AddArr[i] = new int[m];
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    AddArr[i][j] = array[i, j];
                }
            }

            Console.WriteLine("Before");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m - 1; j++)
                {
                    Console.Write(AddArr[i][j] + "  ");
                }
                Console.WriteLine();
                 
            }
            //Сортую окремі рядки
            for (int i = 0; i < n; i++)
            {
                Array.Sort(AddArr[i]);
            }

           
                //Визначаємо в кого найбільше однакових елементів
                for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m - 1; j++)
                {
                    if (AddArr[i][j] == AddArr[i][j + 1])
                        k[i]++;
                }
            }
            Console.WriteLine("Number the same elements");
            for (int i = 0; i < k.Length; i++)
            {
                Console.WriteLine(k[i]);
            }

            Console.WriteLine("Sorting");
            for (int j = 0; j < n - 1; j++)
            {
                for (int i = 0; i < n - 1; i++)
                {
                    if (k[i] > k[i + 1])
                    {
                        Switch(ref AddArr[i], ref AddArr[i + 1]);
                    }
                }
            }
          
            Console.WriteLine("My Result");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m - 1; j++)
                {
                    Console.Write(AddArr[i][j] + "  ");
                }
                Console.WriteLine();
            }
            Console.Read();
        }
    }
}
