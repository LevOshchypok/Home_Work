﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHomeTask4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = new int[8, 8] 
                { {1,2,3,4,5,6,7,8 },
                { 2,5,7,8,9,7,8,9},
                {3,7,9,5,4,7,8,6 },
                { 4,8,7,9,4,5,6,1},
                { 5,7,8,9,4,1,2,6,},
                { 6,8,9,7,5,6,7,8},
                { 7,8,9,2,4,8,6,8},
                { 8,1,7,6,8,9,4,5} };                             
               
            
            #region
            Console.WriteLine("Our array looks like ");
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write(array[i,j] + " ");
                }
                Console.WriteLine();
            }
            #endregion

            for (int i = 0; i < 8; i++)
            {
                Console.WriteLine(Function(array, i)+"    " + "Number of row - " + i);
            }
         
            Console.Read();

        }
        private static bool Function(int[,] arr, int k)
        {
                int[] a = new int[8];
                int[] b = new int[8];

                for (int i = 0; i < 8; i++)
                {
                    a[i] = arr[i, k];
                    b[i] = arr[k, i];
                }
                for (int i = 0; i < 8; i++)
                {
                if (a[i] != b[i])
                {
                    return false;
                }
                }
            return true;          
           
            }
        }
    }

