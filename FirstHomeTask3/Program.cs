﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHomeTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            // number of rows
            int n = 4;
            // number of columns
            int m = 5; 
            int[,] array = new int[n, m];
            Random r = new Random();

            // Set array
            #region
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = r.Next(-20, 30);
                }
            }
            #endregion
            //Array 
            #region
            //Console.WriteLine("Our array looks like ");
            //for (int i = 0; i < n; i++)
            //{
            //    for (int j = 0; j < m; j++)
            //    {
            //        Console.Write(array[i, j] + " ");
            //    }
            //    Console.WriteLine();
            //}
            #endregion
            //Array for sidlova tochka
            int[,] SidArray = new int[4, 5] { { 11,22,7,9,10},
                                                {5,3,6,9,10 },
                                                { 14,13,12,10,11},
                                                 {2,3,4,5,1 } }; 

            //SumOneNegativeElement(array, n, m);
            SidloviTochku(SidArray, n, m);

            Console.Read();
        }
        private static void SumOneNegativeElement(int[,] array, int n, int m)
        {

            int[] Sum = new int[n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (array[i, j] < 0)
                    {
                        for (int g = 0; g < m; g++)
                        {
                            Sum[i] += array[i, j];
                        }
                        break;
                    }
                }               
            }
            //Output
            for (int i = 0; i < Sum.Length; i++)
            {
                if (Sum[i] != 0)
                {
                    Console.WriteLine("Row number {0}   Sum = {1}", (i+1), Sum[i]);
                }
            }
            
        }
        private static void SidloviTochku(int[,] array, int n, int m)
        {
            int min = 0;
            int max = 0;
            int index1 = 0;
            int index2 = 0;
            bool flag;
            bool flag1;

            for (int i = 0; i < n; i++)
            {
                flag = true;
                flag1 = true;
                for (int j = 0; j < m; j++)
                {
                    if (flag)
                    {
                        min = array[i, 0];
                        flag = false;
                    }
                    if (min > array[i, j])
                    {
                        min = array[i, j];
                        index1 = j;
                    }
                }
                for (int g = 0; g < n; g++)
                {
                    if (flag1)
                    {
                       max = array[0, index1];
                        flag1 = false;
                    }
                    if (max < array[g, index1])
                    {
                        max = array[g, index1];
                        index2 = g;
                    }
                    if (min == max)
                    {
                        Console.WriteLine("Coords sidlova tochka ({0},{1}). Value ={2}", index2, index1, max);
                    }
                }
               
            }
        }
    }
}
