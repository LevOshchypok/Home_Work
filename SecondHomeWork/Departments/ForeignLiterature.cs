﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_Home_Work
{
    class ForeignLiterature : Departmen
    {
        public ForeignLiterature(params Book[] b) : base(b)
        {
            base.Name = "ForeignLiterature";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("ForeignLiterature departmen has {0} books", base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }
    }
}
