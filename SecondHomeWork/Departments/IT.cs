﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_Home_Work
{
    class IT : Departmen
    {
        public IT(params Book[] b) : base(b)
        {
            base.Name = "IT";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("IT departmen has {0} books",base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }
    }
}
