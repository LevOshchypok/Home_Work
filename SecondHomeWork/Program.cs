﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace First_Home_Work
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Initialisation
            Library OurLibrary = new Library();

            Departmen IT = new IT();
            Departmen UkrLIt = new UkrainianLiterature();
            Departmen ForLit = new ForeignLiterature();

            Authors Shevchenko = new Authors("Shevchenko");
            Authors Remark = new Authors("Remark");
            Authors Troelsten = new Authors("Troelsten");

            List<Book> ProgBooks = new List<Book> {
                new Book("Programiring_One", Troelsten, 900),
                new Book("Programiring_Two", Troelsten,800),
                new Book("Programiring_Three", Troelsten, 770)};

            List<Book> UkrLitBooks = new List<Book>
            {
                new Book("Kobzar 1", Shevchenko, 700),
                 new Book("Kobzar 2", Shevchenko, 800),
                  new Book("Kobzar 3", Shevchenko, 100),
                   new Book("Kobzar 4", Shevchenko, 256)
            };
            List<Book> ForeignBooks = new List<Book> {
                new Book("Three friends", Remark, 345),
                new Book("Three friends 1", Remark, 256)
            };

            IT.Books.AddRange(ProgBooks);
            UkrLIt.Books.AddRange(UkrLitBooks);
            ForLit.Books.AddRange(ForeignBooks);

            OurLibrary.departmens.Add(IT);
            OurLibrary.departmens.Add(UkrLIt);
            OurLibrary.departmens.Add(ForLit);

            Console.WriteLine(OurLibrary.Count());
            OurLibrary.Display();
            Console.WriteLine(Shevchenko.Count());
            #endregion

            #region Additional tasks
            //Additional task
            Console.WriteLine("Additional task");

            Console.WriteLine();
            MaxNumberBooksInAuthor(Shevchenko, Remark, Troelsten);

            Console.WriteLine();
            MaxNumberBooksInDepartment(IT, UkrLIt, ForLit);

            Console.WriteLine();
            MinPageInBook(OurLibrary);
            #endregion

            #region Linq to object

            Console.WriteLine("\n Aditional task with LINQ \n");
            MaxNumberBooksInAuthorWithLINQ(Shevchenko, Remark, Troelsten);

            Console.WriteLine();
            MaxNumberBooksInDepartmentWithLINQ(IT, UkrLIt, ForLit);

            Console.WriteLine();
            MinPageInBookWithLINQ(OurLibrary);

            #endregion

            #region Create XML
            Console.WriteLine(new string('-', 20));

            XML.Create(OurLibrary);

            XDocument XDoc = XDocument.Load("Library1.xml");
            
            XML.Read(XDoc);

            XML.Change(XDoc, "IT", "Programiring_One", "Abbba");

            XML.Remove(XDoc, "IT", "Programiring_One");

           

            Console.WriteLine(XDoc.ToString());

            #endregion

            Console.ReadKey();
        }
        static void MaxNumberBooksInAuthor(params Authors[] authors)
        {
            if (authors.Length != 0)
            {
                Authors A1 = authors[0];
                for (int i = 0; i < authors.Length; i++)
                {
                    if (A1.CompareTo(authors[i]) < 0)
                    {
                        A1 = authors[i];
                    }
                }
                Console.WriteLine(A1.Name + "Have the most books");
            }
            else { Console.WriteLine("You didn't input ane author"); }
        }
        static void MaxNumberBooksInDepartment(params Departmen[] dep)
        {
            if (dep.Length != 0)
            {
                Departmen D = dep[0];
                for (int i = 0; i < dep.Length; i++)
                {
                    if (D.CompareTo(dep[i]) < 0)
                    {
                        D = dep[i];
                    }
                }
                Console.WriteLine("Department of " + D.Name + " have the most books");
            }
            else { Console.WriteLine("You didn't input ane Department"); }

        }
        static void MinPageInBook(Library lib)
        {
            // Books with the lowest number of pages in each department
            Book[] books = new Book[lib.departmens.Count];
            // Book with the lowest number of pages
            Book book = new Book();
            for (int i = 0; i < lib.departmens.Count; i++)
            {
                book = lib.departmens[i].Books[0];
                for (int j = 0; j < lib.departmens[i].Books.Count; j++)
                {
                    if (book.CompareTo(lib.departmens[i].Books[j]) > 0)
                    {
                        book = lib.departmens[i].Books[j];
                        books[i] = book;
                    }
                }
            }
            book = books[0];
            for (int g = 0; g < books.Length; g++)
            {
                if (book.CompareTo(books[g]) > 0)
                {
                    book = books[g];
                }
            }
            Console.WriteLine("The book with the lowest number of pages is " + book.Name + " with " + book.NumOfPages + " pages.");
        }

        static void MaxNumberBooksInAuthorWithLINQ(params Authors[] authors)
        {
            var Author = authors.OrderByDescending(a => a).First();
            Console.WriteLine("Name of the author with the max number of books is " + Author.Name);
        }
        static void MaxNumberBooksInDepartmentWithLINQ(params Departmen[] dep)
        {
            var Depart = dep.OrderByDescending(d => d).First();
            Console.WriteLine("Name of the department with the max number of books is " + Depart.Name);
        }
        static void MinPageInBookWithLINQ(Library lib)
        {
            List<Book> BooksWithMin = new List<Book>();
            foreach (var dep in lib.departmens)
            {
                var book = dep.Books.OrderBy(b => b).First();
                BooksWithMin.Add(book);
            }
            var BookWithMin = BooksWithMin.OrderBy(b => b).First();
            Console.WriteLine("Book with min number of pages is " + BookWithMin.Name + " author " + BookWithMin.Author.Name);
        }

      
    }
}
