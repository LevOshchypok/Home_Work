﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_Home_Work
{
    class Library:ICountingBooks
    {
        public List<Departmen> departmens;
      
      
        public Library()
        {            
            departmens = new List<Departmen>();
        }

        public override string ToString()
        {
            return string.Format("Our new library");
       }
        public void Display()
        {
            foreach (Departmen d in departmens)
            {
                d.Display();
                Console.WriteLine();
            }
         
        }

        public int Count()
        {
            int Sum = 0;
            
            foreach (Departmen d in departmens)
            {
                Sum += d.Count();
            }
            return Sum;
        }

    }
}
