﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace First_Home_Work
{
    static class XML
    {
        public static void Create(Library lib)
        {                    
            XElement Element = new XElement("Departments");
            List<XElement> elements = new List<XElement>();

            for (int i = 0; i < lib.departmens.Count; i++)
            {
                Element.Add(new XElement(lib.departmens[i].Name));
                elements.Add(Element.Element(lib.departmens[i].Name));
                for (int j = 0; j < lib.departmens[i].Books.Count; j++)
                {
                    string BookName = (lib.departmens[i].Books[j].Name).Replace(' ', '_');
                    string AuthorName = (lib.departmens[i].Books[j].Author.Name);
                    elements[i].Add(new XElement("Book", new XAttribute("Name", BookName), new XAttribute("Author", AuthorName)));
                }
            }
            XDocument XDoc = new XDocument(Element);
           
            XDoc.Save("Library1.xml");
            Console.WriteLine("Document is saved");           
        }
        public static void Read(XDocument XDoc)
        {
            if (XDoc != null && XDoc.Root.HasElements)
            {
                Console.WriteLine(XDoc.Root.Name);

                foreach (var el in XDoc.Root.Elements())
                {
                    Console.WriteLine(new string('=',30));
                    Console.WriteLine(el.Name);
                    foreach (var ele in el.Elements())
                    {
                        Console.WriteLine(new string('-',30));
                        Console.WriteLine(ele.Name);
                        if (ele.Attributes() != null)
                        {
                            foreach (var atr in ele.Attributes())
                            {
                                Console.WriteLine(atr.Name + "  " + atr.Value);
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("There are no document");
            }
        }
        public static void Change(XDocument XDoc, string depName, string bookName, string name)
        {
            if (XDoc != null && XDoc.Root.HasElements)
            {              
                var department = XDoc.Root.Elements().FirstOrDefault(dep => dep.Name == depName);
                var book = department.Elements().FirstOrDefault(b => b.Attribute("Name").Value == bookName);
                if (book != null)
                {
                    book.Attribute("Name").Value = name;
                }
                else
                {
                    Console.WriteLine("There are no this book");
                }
                XDoc.Save("Library1.xml");
            }
        }
        public static void Remove(XDocument XDoc, string depName, string bookName)
        {
            if (XDoc != null && XDoc.Root.HasElements)
            {
                var department = XDoc.Root.Elements().FirstOrDefault(dep => dep.Name == depName);     
                var book = department.Elements().FirstOrDefault(b => b.Attribute("Name").Value == bookName);
                if (book != null)
                {
                    book.Remove();
                }
                else
                {
                    Console.WriteLine("There are no this book");
                }
                XDoc.Save("Library1.xml");
            }
        }
        public static void Remove(XDocument XDoc, string depName)
        {
            if (XDoc != null && XDoc.Root.HasElements)
            {
                var department = XDoc.Root.Elements().FirstOrDefault(dep => dep.Name == depName);
               
                if (department != null)
                {
                    department.Remove();
                }
                else
                {
                    Console.WriteLine("There are no this book");
                }
                XDoc.Save("Library1.xml");
            }
        }
    }
}
