﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.Serialization.Json;
using System.IO;


namespace First_Home_Work
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Initialisation
            Library OurLibrary = new Library();

            Departmen IT = new IT();
            Departmen UkrLIt = new UkrainianLiterature();
            Departmen ForLit = new ForeignLiterature();

            Authors Shevchenko = new Authors("Shevchenko");
            Authors Remark = new Authors("Remark");
            Authors Troelsten = new Authors("Troelsten");

            List<Book> ProgBooks = new List<Book> {
                new Book("Programiring_One", Troelsten, 900),
                new Book("Programiring_Two", Troelsten,800),
                new Book("Programiring_Three", Troelsten, 770)};

            List<Book> UkrLitBooks = new List<Book>
            {
                new Book("Kobzar 1", Shevchenko, 700),
                 new Book("Kobzar 2", Shevchenko, 800),
                  new Book("Kobzar 3", Shevchenko, 100),
                   new Book("Kobzar 4", Shevchenko, 256)
            };
            List<Book> ForeignBooks = new List<Book> {
                new Book("Three friends", Remark, 345),
                new Book("Three friends 1", Remark, 256)
            };

            IT.Books.AddRange(ProgBooks);
            UkrLIt.Books.AddRange(UkrLitBooks);
            ForLit.Books.AddRange(ForeignBooks);

            OurLibrary.departmens.Add(IT);
            OurLibrary.departmens.Add(UkrLIt);
            OurLibrary.departmens.Add(ForLit);

            Console.WriteLine(OurLibrary.Count());
            #endregion

            JSON.Write("Library.json", OurLibrary);

            Library Library1 = new Library();

            JSON.Read("Library.json", Library1);
           
            Library1.Display();
                  
            Console.ReadKey();
        } 
    } 
}
