﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.Serialization.Json;
using System.IO;

namespace First_Home_Work
{
    static class JSON
    {
        static DataContractJsonSerializer jsonFormatter;
        static JSON()
        {
            jsonFormatter = new DataContractJsonSerializer(typeof(Library));
        }
        public static void Write(string nameOfFile, Library lib)
            {
          
            using (FileStream fs = new FileStream(nameOfFile, FileMode.OpenOrCreate))
            {
                jsonFormatter.WriteObject(fs, lib);
            }

            Console.WriteLine("Your data was written in the file " + nameOfFile);
        }
        public static void Read(string nameOfFile, Library lib)
        {
            FileStream fs = new FileStream(nameOfFile, FileMode.OpenOrCreate);
                           
                lib = (Library)jsonFormatter.ReadObject(fs);
                Console.WriteLine("Your data was read from the file " + nameOfFile);
                lib.Display();
                fs.Close();           
        }
    }
}
