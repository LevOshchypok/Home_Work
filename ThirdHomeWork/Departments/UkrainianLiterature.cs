﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace First_Home_Work
{
    [DataContract]
    class UkrainianLiterature : Departmen
    {
        public UkrainianLiterature(params Book[] b) : base(b)
        {
            base.Name = "UkrainianLiterature";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("UkrainianLiterature departmen has {0} books", base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }

    }
}
