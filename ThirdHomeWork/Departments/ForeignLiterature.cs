﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace First_Home_Work
{
    [DataContract]
    class ForeignLiterature : Departmen
    {
        public ForeignLiterature(params Book[] b) : base(b)
        {
            base.Name = "ForeignLiterature";
        }
        public override int NumOfBooks()
        {
            //Console.WriteLine("ForeignLiterature departmen has {0} books", base.NumOfBooks().ToString());
            return base.NumOfBooks();
        }
    }
}
