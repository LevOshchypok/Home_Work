﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace First_Home_Work
{
    [DataContract]
    [KnownType(typeof(IT))]
    [KnownType(typeof(UkrainianLiterature))]
    [KnownType(typeof(ForeignLiterature))]
    abstract class Departmen:IComparable<Departmen>,ICountingBooks
    {
        public string Name { get; protected set; }
        [DataMember]
        public List<Book> Books { get; set; }
       

        public Departmen() { }
        public Departmen(string name) : this(name, null) { }
        public Departmen(params Book[] b) : this("", b) { }
        public Departmen(string name,params Book[] b)
        {          
            Books = new List<Book>();
            this.Name = name;
            this.Books.AddRange(b);
        }

        public virtual int NumOfBooks()
        {
            return this.Books.Count;
        }
        public override string ToString()
        {
            return string.Format("The {0} has {1} books", Name, this.NumOfBooks().ToString());
        }
        public void Display()
        {
            Console.WriteLine(this.ToString());
            Console.WriteLine("Such AS");
            foreach (Book b in Books)
            {
                Console.WriteLine(b.ToString());
            }
        }

        public int CompareTo(Departmen other)
        {
            return this.Books.Count.CompareTo(other.Books.Count);
        }

        public int Count()
        {
            return Books.Count;
        }
    }  
}
