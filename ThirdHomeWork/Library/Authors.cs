﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;


namespace First_Home_Work
{
    [DataContract]
    class Authors : IComparable<Authors>, ICountingBooks
    {
        [DataMember]
        public string Name { get; private set; }
        public int NumOfBooks { get; set; }
        public Authors(string name)
        {
            Name = name;
        }

        public int CompareTo(Authors other)
        {
            return this.NumOfBooks.CompareTo(other.NumOfBooks);
        }

        public int Count()
        {
            return NumOfBooks;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
